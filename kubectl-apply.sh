#!/bin/bash
# Files are ordered in proper order with needed wait for the dependent custom resource definitions to get initialized.
# Usage: bash kubectl-apply.sh

kubectl apply -f namespace.yml
kubectl apply -f registry/
kubectl apply -f conferences/
kubectl apply -f gateway/
kubectl apply -f tickets/
kubectl apply -f uaa/
kubectl apply -f console/
